from tkinter import*
import openpyxl
from Week import *
wb = openpyxl.load_workbook('fix.xlsm')
ws = wb['Fixture List']
def weeklyfixture(week_no):
    window5 = Tk()
    window5.title('Week '+str(week_no)+' Fixtures')
    teamindex = ((week_no*8)+1)-8
    teams = []
    isplayed = True
    for i in range(teamindex,teamindex + 8):
        if i % 2 == 1:
            teams.append(Label(window5,text = ws.cell(row = i, column = 1).value,bg = '#c6e0b4',width = 30))
            teams[-1].grid(row = i,column = 0,sticky = 'E')
            teams.append(Label(window5,text = ws.cell(row = i,column = 3).value,bg = '#c6e0b4',width = 2))
            if ws.cell(row = i,column = 3).value != "NA":
                teams[-1].grid(row = i,column = 1)
            else:
                isplayed = False
            teams.append(Label(window5,text = 'Vs',bg = '#c6e0b4',width = 30))
            teams[-1].grid(row = i,column = 2)
            teams.append(Label(window5,text = ws.cell(row = i,column = 4).value,bg = '#c6e0b4',width = 2))
            if ws.cell(row = i,column = 4).value != 'NA':
                teams[-1].grid(row = i,column = 3)
            teams.append(Label(window5,text = ws.cell(row = i, column = 5).value,bg = '#c6e0b4',width = 30))
            teams[-1].grid(row = i,column = 4,sticky = 'W')
        else:
            teams.append(Label(window5,text = ws.cell(row = i, column = 1).value,bg="#e2efda",width = 30))
            teams[-1].grid(row = i,column = 0,sticky = 'E')
            teams.append(Label(window5,text = ws.cell(row = i,column = 3).value,bg = '#e2efda',width = 2))
            if ws.cell(row = i,column = 3).value != 'NA':
                teams[-1].grid(row = i,column = 1)
            else:
                isplayed = False
            teams.append(Label(window5,text = 'Vs',bg="#e2efda",width = 30))
            teams[-1].grid(row = i,column = 2)
            teams.append(Label(window5,text = ws.cell(row = i,column = 4).value,bg = '#e2efda',width = 2))
            if ws.cell(row = i,column = 3).value != 'NA':
                teams[-1].grid(row = i,column = 3)
            teams.append(Label(window5,text = ws.cell(row = i, column = 5).value,bg="#e2efda",width = 30))
            teams[-1].grid(row = i,column = 4,sticky = 'W')
    if isplayed:
        pass
    else:
        window5.title('Week '+str(week_no)+' Fixtures(Not Played)')

    window5.mainloop()

 