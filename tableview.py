from tkinter import *
import xlrd 
from refresh import *
def tableView(window,index,height):
    wb = xlrd.open_workbook('fix.xlsm')
    ws = wb.sheet_by_index(index)
    r = 0
    rowVal=[]
    while  r<height-1:
        for cell in ws.row(r):
            rowVal.append(cell.value)
        r += 1
    index = 0
    while index < len(rowVal):
        x = rowVal[index]
        if type(x) == float:
            rowVal[index] = int(x)
        index += 1
    j = 1
    x = rowVal.count('')
    for i in range(x):
        rowVal.remove('')
    k = 0
    for i in range(height-1):
        for j in range(10):
            s = str(rowVal[k])
            if i == 0 :
                if len(s) > 3:
                    Label(window,text=s,width = 45,height=2,bg="#548235",fg="white").grid(row=i ,column = j)
                else:
                    Label(window,text=s,width = 15,height=2,bg = "#548235",fg="white").grid(row=i ,column = j)
            elif i%2 == 0:
                if len(s) > 3:
                    Label(window,text=s,width = 45,height=2,bg="#e2efda").grid(row=i ,column = j)
                else:
                    Label(window,text=s,width = 15,height=2,bg = "#e2efda").grid(row=i ,column = j)
            else:
                if len(s) > 3:
                    Label(window,text=s,width = 45,height=2,bg="#c6e0b4").grid(row=i ,column = j)
                else:
                    Label(window,text=s,width = 15,height=2,bg = "#c6e0b4").grid(row=i ,column = j)
            k += 1  