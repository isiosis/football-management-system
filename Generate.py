from openpyxl import *
from tkinter import *
import random
def Randomize(window):
    progress = Label(window,text = 'Generating Matches...This may take some time').pack()
    wb = load_workbook("fix.xlsm")#import the workbook
    ws = wb["Standings"]
    ws2 = wb["Fixture List"]#select the fixtures sheeet 
    teams = ['Ethiopian Coffee S.C.','Wolaitta Dicha S.C.','Dedebit F.C.','Debub Police S.C.','Welwalo Adigrat University F.C.','Fasil Kenema S.C.',
      'Shire Endaselassie F.C.','Adama City F.C.','Jimma Abba Jifar F.C.','Saint George S.C.','Bahir Dar Kenema F.C.','Sidama Coffee S.C.','Mekele 70 Enderta S.C.','Hawassa City S.C.','Defence Force S.C.','Dire Dawa City S.C.']
    
    fixtures = []
    while len(fixtures) < 240:#randomly select a pair of teams from the selected teams, no ordered pairing appears twice
        x = random.randint(0,15)
        y = random.randint(0,15)
        if fixtures.count([teams[x],teams[y]]) == 0 and x != y:
            fixtures.append([teams[x],teams[y]])
    
    
    orderedfixtures =[]
    for i in range(30):
        orderedfixtures.append([])
    
    index = 0
    while True: 
        #add the first eight fixtures
        for i in range(8):
            orderedfixtures[index].append(fixtures[i])
        temp = []
    
        for i in orderedfixtures[index]:
            for j in i:
                temp.append(j)
        count = []
        for i in teams:
            if temp.count(i) > 1:
                count.append(i)
        if len(count) == 0: # only append the fixtures when there is no repetition
            # remove the appended elements from fixtures
            for i in range(8):
                fixtures.pop(0)
            index += 1       
        else:# else reset orderedfixtures
            orderedfixtures[index] = []
            random.shuffle(fixtures)
            continue
        if fixtures == []:
            break
        else:
            print(orderedfixtures)
            continue
    #Adding the fixtures to the fix.xlsx
    row = 1
    for week in orderedfixtures:
        for match in week:
            ws2.cell(row = row,column = 1).value = match[0]
            ws2.cell(row = row,column = 5).value = match[1]
            row += 1
          
    wb.save('fix.xlsm')
    progress['text'] = 'Matches Succcefully Generated!!!'

        


    
            
            
    
            
            


