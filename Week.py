import tkinter.messagebox
import tkinter.simpledialog
import openpyxl
from Teams import *
from simulate import *
import win32com.client
import os
wb = openpyxl.load_workbook('fix.xlsm',keep_vba = True)
ws = wb['Fixture List']
class Weekly:
    def __init__(self,week_no):
        self.window5 = Tk()  
        self.week_no = week_no
        self.window5.title('Week '+str(self.week_no)+' Fixtures')
        self.teamindex = ((self.week_no*8)+1)-8
        self.teams = []
        self.variables = []
        self.entrys = []
        for i in range(16):
            intVar = IntVar(self.window5)
            self.variables.append(intVar)
        self.frame = Frame(self.window5)
        self.frame.pack()
        self.framebot = Frame(self.window5)
        self.framebot.pack()   
        j = 2
        for i in range(self.teamindex,self.teamindex + 8):
            self.teams.append(Label(self.frame,text = ws.cell(row = i, column = 1).value,width = 30))
            self.teams[-1].grid(row = i,column = 0,sticky = 'E')
            self.entrys.append(Entry(self.frame,textvariable = self.variables[j-2],width = 2))
            self.entrys[-1].grid(row = i,column = 1)
            Label(self.frame,text = 'vs').grid(row = i,column = 2)
            self.entrys.append(Entry(self.frame,textvariable = self.variables[j-1],width = 2))
            self.entrys[-1].grid(row = i,column= 3)
            self.teams.append(Label(self.frame,text = ws.cell(row = i, column = 5).value,width = 30))
            self.teams[-1].grid(row = i,column = 4,sticky = 'W')
            j += 2
        Button(self.framebot,text = '<<Previous',command  = lambda:self.previous(self.frame,self.week_no),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 0,padx = 10,pady = 5)
        Button(self.framebot,text = 'Simulate',command = lambda:simulate(self.week_no),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 1,padx = 10,pady = 5)
        Button(self.framebot,text = 'Update Weekly Stats',command = lambda:teams(self.week_no),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 2,padx = 10,pady = 5)
        Button(self.framebot,text = 'Update Scores',command = lambda:self.update(self.teamindex),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 3,padx = 10,pady = 5)
        Button(self.framebot,text = 'Next>>',command = lambda:self.next(self.frame,self.week_no),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 4,padx = 10,pady = 5)
        self.window5.mainloop()
    def update(self,teamindex):
        scores = self.getScores()
        y = True
        for i in scores:
            if i < 0 :
                y = False
                break
            else:
                y = True
        if y:
            wb = openpyxl.load_workbook('fix.xlsm',keep_vba = True)
            ws = wb['Fixture List']
            row = teamindex
            while row < teamindex + 8:
                    for j in range(2):
                        for col in range(3,5):
                            if col == 3 and j == 0:
                                ws.cell(row = row,column = 3).value = scores[0]
                            if col == 4 and j == 1:
                                ws.cell(row = row,column = 4).value = scores[1]
                    for k in range(2):
                        scores.pop(0)
                    row += 1
            wb.save('fix.xlsm')
            tkinter.messagebox.showinfo('Update','Score Updates Successful')
        else:
            tkinter.messagebox.showinfo('Update','You Have Entered An Invalid Value')
    def getScores(self):
        scores = []
        for i in range(len(self.entrys)):
            scores.append(self.variables[i].get())
        return scores
    def previous(self,frame,week_no):
        if week_no > 1:
            previousweek = week_no - 1
            teamindex = ((previousweek*8)+1) - 8
            self.teams = []
            self.variables = []
            for i in range(16):
                intVar = IntVar(self.window5)
                self.variables.append(intVar)
            self.entrys = []
            for widgets in frame.winfo_children():
                widgets.destroy()
            self.window5.title('Week '+str(previousweek)+' Fixtures')
            j = 2
            for i in range(teamindex,teamindex + 8):
                self.teams.append(Label(frame,text = ws.cell(row = i, column = 1).value,width = 30))
                self.teams[-1].grid(row = i,column = 0,sticky = 'E')
                self.entrys.append(Entry(frame,textvariable = self.variables[j-2],width = 2))
                self.entrys[-1].grid(row = i,column = 1)
                Label(frame,text = 'vs').grid(row = i,column = 2)
                self.entrys.append(Entry(frame,textvariable = self.variables[j-1],width = 2))
                self.entrys[-1].grid(row = i,column= 3)
                self.teams.append(Label(frame,text = ws.cell(row = i, column = 5).value,width = 30))
                self.teams[-1].grid(row = i,column = 4,sticky = 'W')
                j += 2
            Button(self.framebot,text = '<<Previous',command = lambda:self.previous(frame,previousweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 0,padx = 10,pady = 5)
            Button(self.framebot,text = 'Simulate',command = lambda:simulate(previousweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 1,padx = 10,pady = 5)
            Button(self.framebot,text = 'Update Weekly Stats',command = lambda:teams(previousweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 2,padx = 10,pady = 5)
            Button(self.framebot,text = 'Update Scores',command = lambda:self.update(teamindex),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 3,padx = 10,pady = 5)
            Button(self.framebot,text = 'Next>>',command = lambda:self.next(frame,previousweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 4,padx = 10,pady = 5)
        else:
            pass        

    def next(self,frame,week_no):
        if week_no < 30:
            nextweek = week_no + 1
            teamindex = ((nextweek*8)+1) - 8
            self.teams = []
            self.variables = []
            for i in range(16):
                intVar = IntVar(self.window5)
                self.variables.append(intVar)
            self.entrys = []
            for widgets in frame.winfo_children():
                widgets.destroy()
            self.window5.title('Week '+str(nextweek)+' Fixtures')
            j = 2
            for i in range(teamindex,teamindex + 8):
                self.teams.append(Label(frame,text = ws.cell(row = i, column = 1).value,width = 30))
                self.teams[-1].grid(row = i,column = 0,sticky = 'E')
                self.entrys.append(Entry(frame,textvariable = self.variables[j-2],width = 2))
                self.entrys[-1].grid(row = i,column = 1)
                Label(frame,text = 'vs').grid(row = i,column = 2)
                self.entrys.append(Entry(frame,textvariable = self.variables[j-1],width = 2))
                self.entrys[-1].grid(row = i,column= 3)
                self.teams.append(Label(frame,text = ws.cell(row = i, column = 5).value,width = 30))
                self.teams[-1].grid(row = i,column = 4,sticky = 'W')
                j += 2
            Button(self.framebot,text = '<<Previous',command = lambda:self.previous(frame,nextweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 0,padx = 10,pady = 5)
            Button(self.framebot,text = 'Simulate',command = lambda:simulate(nextweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 1,padx = 10,pady = 5)
            Button(self.framebot,text = 'Update Weekly Stats',command = lambda:teams(nextweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 2,padx = 10,pady = 5)
            Button(self.framebot,text = 'Update Scores',command = lambda:self.update(teamindex),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 3,padx = 10,pady = 5)
            Button(self.framebot,text = 'Next>>',command = lambda:self.next(frame,nextweek),bg = '#00331f',fg = '#f2f2f2').grid(row = 0,column = 4,padx = 10,pady = 5)
        else:
            pass






        
