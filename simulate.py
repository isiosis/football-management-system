import xlrd
import openpyxl
from openpyxl import load_workbook
import random
from tkinter import *

def simulate(week_no):
    wb = load_workbook('fix.xlsm',keep_vba=True)
    ws = wb['Fixture List']
    rb = xlrd.open_workbook('fix.xlsm')
    rs = rb.sheet_by_name('Fixture List')
    entries = []
    teams = []
    for i in range((((week_no-1)*8)+1),week_no*8+1):
        t1 = ws.cell(row=i,column=1).value
        t2 = ws.cell(row=i,column=5).value
        entries.append(random.randint(0,5))
        entries.append(random.randint(0,3))
        teams.append(t1)
        teams.append(t2)
    i = (week_no-1)*8+1
    k = 3

    for entry in entries:
            ws.cell(row=i,column=k).value =entry
            k += 1
            if k == 5:
                k = 3
                i +=1
            wb.save('fix.xlsm')
    j = 0 
    for i in teams:
        ws2 = wb[i]
        rs2 =rb.sheet_by_name(i)
        score = entries[j]
        goal = []
        if score != 0:
            while sum(goal) < score:
                z =random.randint(1,score)
                if (sum(goal) + z) <= score:
                    goal.append(z)
        for i in goal:
            ro = random.randint(2,rs2.nrows)
            x = rs2.cell(rowx =ro-1,colx=3).value 
            x = int(x) + i
            ws2.cell(row =ro,column=4).value = x
        j += 1
        wb.save('fix.xlsm')
    
    j = 0
    for i in teams:
        ws2 = wb[i]
        rs2 =rb.sheet_by_name(i)
        score = entries[j]
        goal = []
        if score != 0:
            while sum(goal) < score:
                z =random.randint(1,score)
                if (sum(goal) + z) <= score:
                    goal.append(z)
        for i in goal:
            ro = random.randint(2,rs2.nrows)
            x = rs2.cell(rowx =ro-1,colx=4).value
            if x != '':
                x += i
            else:
                x = i
            ws2.cell(row =ro,column=5).value = x
        j += 1
        wb.save('fix.xlsm')
    
    yellow = 4
    for i in teams:
        ws2 = wb[i]
        rs2 =rb.sheet_by_name(i)
        yellows = []
        if score != 0:
            while sum(yellows) < yellow:
                z =random.randint(1,yellow)
                if (sum(yellows) + z) <= yellow:
                    yellows.append(z)
        for i in yellows:
            ro = random.randint(2,rs2.nrows)
            x = rs2.cell(rowx =ro-1,colx=6).value
            if  x != '':
                if int(x) > 2:
                    continue
                else:
                    x += i
                    ws2.cell(row =ro,column=7).value = x
            else:
                    x = i
                    ws2.cell(row =ro,column=7).value = x
        wb.save('fix.xlsm')
    red = 1
    for i in teams:
        ws2 = wb[i]
        rs2 =rb.sheet_by_name(i)
        reds = []
        if score != 0:
            while sum(reds) < red:
                z =random.randint(1,red)
                if (sum(reds) + z) <= red:
                    reds.append(z)
        for i in reds:
            ro = random.randint(2,rs2.nrows)
            x = rs2.cell(rowx =ro-1,colx=5).value
            if x != '':
                x += i
            else:
                x = i
            if x == 1:
                continue
            else:
                
                ws2.cell(row =ro,column=6).value = x
        wb.save('fix.xlsm')
    tkinter.messagebox.showinfo('Simulate','Matches Simulated Successfuly')
   