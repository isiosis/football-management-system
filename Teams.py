from tkinter import *
import openpyxl
import tkinter.messagebox
wb = openpyxl.load_workbook('fix.xlsm',keep_vba = True)
class teams:
    def __init__(self,weekno):
        self.weekno = weekno
        root = Tk()
        root.title('Updating TeamSheet for Week-'+ str(weekno))
        frame = Frame(root)
        frame.pack(side = LEFT)
        rosterframe = Frame(root)
        rosterframe.pack(side = RIGHT)
        Button1 = Button(root,text = 'Ethiopian Coffee S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Ethiopian Coffee S.C.',self.weekno)).pack()
        Button2 = Button(root,text = 'Wolaitta Dicha S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Wolaitta Dicha S.C.',self.weekno)).pack()
        Button3 = Button(root,text = 'Dedebit F.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Dedebit F.C.',self.weekno)).pack()
        Button4 = Button(root,text = 'Debub Police S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Debub Police S.C.',self.weekno)).pack()
        Button5 = Button(root,text = 'Welwalo Adigrat University F.C.',width = 50,command = lambda: self.Sheet(rosterframe,'Welwalo Adigrat University F.C.',self.weekno)).pack()
        Button6 = Button(root,text = 'Fasil Kenema S.C.',width = 50,command = lambda : self.Sheet(rosterframe,'Fasil Kenema S.C.',self.weekno)).pack()
        Button7 = Button(root,text = 'Shire Endeselassie F.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Shire Endeselassie F.C.',self.weekno)).pack()
        Button8 = Button(root,text = 'Adama City F.C.',width = 50,command = lambda: self.Sheet(rosterframe,'Adama City F.C.',self.weekno)).pack()
        Button9 = Button(root,text = 'Jimma Abba Jifar F.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Jimma Abba Jifar F.C.',self.weekno)).pack()
        Button10 = Button(root,text = 'Saint George S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Saint George S.C.',self.weekno)).pack()
        Button11 = Button(root,text = 'Bahir Dar Kenema F.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Bahir Dar Kenema F.C.',self.weekno)).pack()
        Button12 = Button(root,text = 'Sidama Coffee S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Sidama Coffee S.C.',self.weekno)).pack()
        Button13 = Button(root,text = 'Mekele 70 Enderta S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Mekele 70 Enderta S.C.',self.weekno)).pack()
        Button14 = Button(root,text = 'Hawassa City S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Hawassa City S.C.',self.weekno)).pack()
        Button15 = Button(root,text = 'Defence Force S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Defence Force S.C.',self.weekno)).pack()
        Button16 = Button(root,text = 'Dire Dawa City S.C.',width = 50,command = lambda:self.Sheet(rosterframe,'Dire Dawa City S.C.',self.weekno)).pack()
        root.mainloop()
    def Sheet(self,frame,teamname,weekno):
        for widgets in frame.winfo_children():
            widgets.destroy()
        specificTeam(frame,teamname,weekno)
    

#GUI with specific team name and weekno
class specificTeam:
    def __init__(self,frame,teamname,weekno):#GUI with three fields 
        self.teamname = teamname
        ws = wb[self.teamname]
        #getting the start index to update function
        self.weekno = weekno
        l = 1
        length = 0
        while ws.cell(row = l,column = 1).value != None:
            length += 1
            l += 1
        start = length + 2
        self.index = start + ((self.weekno-1)*length) 
                
        # five lists to store five widgets
        self.names = []
        self.goals = [] 
        self.assists = []
        self.redcards = []
        self.yellowcards = []
        #four lists to store four entry variables
        self.goalvariables = []
        self.assistsvariables = []
        self.yellowvariables = []
        self.redvariables = []
        i = 2
        while ws.cell(row = i,column = 1).value != None:
            goals = IntVar(frame)
            assists = IntVar(frame)
            yellowcards = IntVar(frame)
            redcards = IntVar(frame)
            self.goalvariables.append(goals)
            self.assistsvariables.append(assists)
            self.yellowvariables.append(yellowcards)
            self.redvariables.append(redcards)
            i += 1
        #Displays the widgets
        i = 1
        while ws.cell(row = i,column = 1).value != None:
            if i == 1:
                Label(frame, text = 'Player Name').grid(row = i,column = 1)
                Label(frame, text = 'Goals').grid(row = i,column = 2)
                Label(frame, text = 'Assits').grid(row = i, column = 3)
                Label(frame, text = 'RedC.').grid(row = i,column = 4)
                Label(frame, text = 'YellowC.').grid(row = i,column = 5)
            else:
                self.names.append(Label(frame, text = ws.cell(row = i,column = 2).value))
                self.names[-1].grid(row = i,column = 1)
                self.goals.append(Entry(frame,textvariable = self.goalvariables[i-2]))
                self.goals[-1].grid(row = i,column = 2)
                self.assists.append(Entry(frame,textvariable = self.assistsvariables[i-2]))
                self.assists[-1].grid(row = i,column = 3)
                self.redcards.append(Entry(frame,textvariable = self.redvariables[i-2]))
                self.redcards[-1].grid(row = i,column = 4)
                self.yellowcards.append(Entry(frame,textvariable = self.yellowvariables[i-2]))
                self.yellowcards[-1].grid(row = i,column = 5)
            i += 1
        Button(frame,text = 'Update',command = self.update).grid(row = ws.max_row + 1,column = 3,pady = 10)
    def update(self):
        ws = wb[self.teamname]
        #updates the information in a specific order
        goals = self.getStat(self.goalvariables)
        assists = self.getStat(self.assistsvariables)
        redcards = self.getStat(self.redvariables)
        yellowcards = self.getStat(self.yellowvariables)
        i = self.index
        j = 2
        while ws.cell(row = i,column = 1).value != None:
            ws.cell(row = i, column = 4).value = goals[j-2]
            ws.cell(row = i,column = 5).value = assists[j-2]
            ws.cell(row = i,column = 6).value = redcards[j-2]
            ws.cell(row = i,column = 7).value = yellowcards[j-2]
            i += 1
            j += 1
        
        wb.save('fix.xlsm')
        tkinter.messagebox.showinfo('Update','Stat Updates Successful')
    def getStat(self,variable):#gets the stats for each information returning a list
        stat = []
        for i in range(len(variable)):
            stat.append(variable[i].get())
        return stat
            
        
        
        
