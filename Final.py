from tkinter import*
import openpyxl
from Week_1 import *
from tableview import *
from recordview import *
from viewsquads import *
from Generate import *
from Week import *
from simulate import *
import cv2
import PIL.Image, PIL.ImageTk
from refresh import *
wb = openpyxl.load_workbook('fix.xlsm')
ws = wb['Fixture List']
class Access:
    def __init__(self):
        self.season = 0 
        self.window = Tk()
        self.window.resizable(0,0)
        self.window.title("Login Window")
        cv_img = cv2.cvtColor(cv2.imread("back.png"), cv2.COLOR_BGR2RGB)
        height,width,no_channels = cv_img.shape
        photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
        canvas = Canvas(self.window,width=width,height=height)
        canvas.create_image(0, 0, image=photo, anchor=tkinter.NW)
        canvas.pack()
        self.window.geometry('1300x830+100+10')
        self.User = StringVar()
        self.Password = StringVar()
        Label(self.window,text="Welcome To The Ethiopian Premier League Management System").pack()
        Label(self.window,text="Username: ").pack()
        Entry(self.window,textvariable=self.User).pack()
        Label(self.window,text="Password: ").pack()
        Entry(self.window,textvariable=self.Password,show="*").pack()
        button2 = Button(self.window,text = "Login",command=self.CheckAll,font = 'Times 10 bold',bg = 'teal').pack()
        self.count = 0
        self.window.mainloop() # Create an event loop
    def CheckAll(self):
        UserAccounts = ['abe','abe']
        AdminAccounts = ['admin','admin']
        ITAccounts = ['it','it1']
        l = Label(self.window,text="Incorrect UserName or Password")
        if self.User.get() in UserAccounts:
           
            if self.Password.get() in UserAccounts and self.count < 7:
                x = UserAccounts.index(self.User.get()) - UserAccounts.index(self.Password.get())
                if (x ==-1) or(x==0):
                    self.UserAccess()
                    l.destroy()
                    self.count = 1
            else:
                l.pack()
                self.count += 1
        else:
            self.count += 1
        print(self.count)
        if self.count == 7:
            l['text'] = 'You are locked out of your account. Contact your IT professional to reset your account'
        

        if self.User.get() in AdminAccounts:
            
            if self.Password.get() in AdminAccounts:
                x = AdminAccounts.index(self.User.get()) - AdminAccounts.index(self.Password.get())
                if (x ==-1) or (x == 0):
                    self.AdminAccess()
                    l.destroy()
            else:
                l.pack()
        if self.User.get() in ITAccounts:
            if self.Password.get() in ITAccounts:
                x = ITAccounts.index(self.User.get()) - ITAccounts.index(self.Password.get())
                if (x ==-1) or (x == 0):
                    self.ITAccess()
                    l.destroy()
            else:
               l.pack()
        else:
            l.pack()
    def AdminAccess(self):
        self.window3 = Tk()
        self.window3.title('Admin User')
        self.window3.geometry('1300x830+100+10')
        self.window.withdraw()
        menubar = Menu(self.window3)
        self.window3.config(menu = menubar)
        ope = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Menu", menu = ope)
        ope.add_command(label = "Update scores",command = self.USc)
        ope.add_command(label = "Logout",command=self.back)
        exitmenu = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Exit", menu = exitmenu)
        exitmenu.add_command(label = "Quit", command = self.window3.destroy)
        Label(self.window3,text = "                 Welcome to Ethiopian Premier League\nThe Ethiopian Premier League is the top association football division in Ethiopi\na. Regulated by the Ethiopian Football Federation, it was created in 1997 (1990 \nE.C.) replacing the former first division (est.1944). Contested by sixteen clubs\n, it operates on a system of promotion and relegation with the other secondary a\nnd tertiary leagues in Ethiopia. The league has been an annual competition since\n the 1997-98 season with Saint George S.C. emerging as the country's leading clu\nb in this era with 14 titles (29 first division titles overall).                                         ",font=('Times',25)).pack()
        if self.season == 0:
            Label(self.window3,text = "We have already generated the season's fixtures for you\nClick the button bellow to generate random matches",font=('Times',25)).pack()
            Button(self.window3,text = 'Begin the season By Setting the Season fixtures',bg = '#00331f',fg = '#f2f2f2',command = lambda:Randomize(self.window3),height=5,width=50).pack()
            self.season = 1
        self.window3.mainloop()
    def ITAccess(self):
        self.window1=Tk()
        self.window.withdraw()
        self.window1.geometry('1300x830+100+10')
        Label(self.window1,text="Welcome To Your IT Maintainance Account").grid(row=1,column=1)

        self.window1.mainloop()

    def back(self):
        self.window.deiconify()
        self.window3.withdraw()
        self.window4.withdraw()
        self.window2.withdraw()
        self.window10.withdraw()
    def back1(self):
        self.window.deiconify()
        self.window2.withdraw()
        self.window8.withdraw()
        self.window10.withdraw()
    def back2(self):
        self.window.deiconify()
        self.window5.withdraw()
    def back3(self):
        self.window4.destroy()
        self.window3.deiconify()
    def USc(self):
        self.window4 = Tk()
        self.window4.title("All Fixtures")
        self.window4.geometry('1300x830+100+10')
        self.window3.withdraw()
        menubar = Menu(self.window4)
        self.window4.config(menu = menubar)
        ope = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Menu", menu = ope)
        ope.add_command(label = "Main Menu",command=self.back3)
        exitmenu = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Exit", menu = exitmenu)
        exitmenu.add_command(label = "Quit", command = self.window4.destroy)
        week01 = Button(self.window4,text = "Week01",command= lambda :Weekly(1),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 1,padx =20,pady=15)
        week02 = Button(self.window4,text = "Week02",command= lambda :Weekly(2),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 1,padx =20,pady=15)
        week03 = Button(self.window4,text = "Week03",command= lambda :Weekly(3),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 1,padx =20,pady=15)
        week04 = Button(self.window4,text = "Week04",command= lambda :Weekly(4),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 1,padx =20,pady=15)
        week05 = Button(self.window4,text = "Week05",command= lambda :Weekly(5),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 1,padx =20,pady=15)
        week06 = Button(self.window4,text = "Week06",command= lambda :Weekly(6),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 1,padx =20,pady=15)
        week07 = Button(self.window4,text = "Week07",command= lambda :Weekly(7),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 2,padx =20,pady=15)
        week08 = Button(self.window4,text = "Week08",command= lambda :Weekly(8),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 2,padx =20,pady=15)
        week09 = Button(self.window4,text = "Week09",command= lambda :Weekly(9),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 2,padx =20,pady=15)
        week10 = Button(self.window4,text = "Week10",command= lambda :Weekly(10),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 2,padx=20,pady=15)
        week11 = Button(self.window4,text = "Week11",command= lambda :Weekly(11),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 2,padx=20,pady=15)
        week12 = Button(self.window4,text = "Week12",command= lambda :Weekly(12),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 2,padx=20,pady=15)
        week13 = Button(self.window4,text = "Week13",command= lambda :Weekly(13),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 3,padx=20,pady=15)
        week14 = Button(self.window4,text = "Week14",command= lambda :Weekly(14),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 3,padx=20,pady=15)
        week15 = Button(self.window4,text = "Week15",command= lambda :Weekly(15),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 3,padx=20,pady=15)
        week16 = Button(self.window4,text = "Week16",command= lambda :Weekly(16),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 3,padx=20,pady=15)
        week17 = Button(self.window4,text = "Week17",command= lambda :Weekly(17),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 3,padx=20,pady=15)
        week18 = Button(self.window4,text = "Week18",command= lambda :Weekly(18),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 3,padx=20,pady=15)
        week19 = Button(self.window4,text = "Week19",command= lambda :Weekly(19),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 4,padx=20,pady=15)
        week20 = Button(self.window4,text = "Week20",command= lambda :Weekly(20),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 4,padx=20,pady=15)
        week21 = Button(self.window4,text = "Week21",command= lambda :Weekly(21),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 4,padx=20,pady=15)
        week22 = Button(self.window4,text = "Week22",command= lambda :Weekly(22),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 4,padx=20,pady=15)
        week23 = Button(self.window4,text = "Week23",command= lambda :Weekly(23),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 4,padx=20,pady=15)
        week24 = Button(self.window4,text = "Week24",command= lambda :Weekly(24),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 4,padx=20,pady=15)
        week25 = Button(self.window4,text = "Week25",command= lambda :Weekly(25),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 5,padx=20,pady=15)
        week26 = Button(self.window4,text = "Week26",command= lambda :Weekly(26),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 5,padx=20,pady=15)
        week27 = Button(self.window4,text = "Week27",command= lambda :Weekly(27),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 5,padx=20,pady=15)
        week28 = Button(self.window4,text = "Week28",command= lambda :Weekly(28),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 5,padx=20,pady=15)
        week29 = Button(self.window4,text = "Week29",command= lambda :Weekly(29),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 5,padx=20,pady=15)
        week30 = Button(self.window4,text = "Week30",command= lambda :Weekly(30),width = 30,height=5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 5,padx=20,pady=15)
        self.window4.mainloop()
    def USt(self):
        self.window10 = Tk()
        self.window2.withdraw()
        self.window10.title("Current Season 2011")
        self.window10.geometry('1300x830+100+10')
        menubar = Menu(self.window10)
        self.window10.config(menu = menubar)
        ope = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Menu", menu = ope)
        ope.add_command(label = "Logout",command=self.back1)
        exitmenu = Menu(menubar, tearoff = 0)
        menubar.add_cascade(label = "Exit", menu = exitmenu)
        exitmenu.add_command(label = "Quit", command = self.window10.destroy)
        week01 = Button(self.window10,text = "Week01",command= lambda :weeklyfixture(1),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 1,padx = 20,pady= 15)
        week02 = Button(self.window10,text = "Week02",command= lambda :weeklyfixture(2),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 1,padx = 20,pady= 15)
        week03 = Button(self.window10,text = "Week03",command= lambda :weeklyfixture(3),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 1,padx = 20,pady= 15)
        week04 = Button(self.window10,text = "Week04",command= lambda :weeklyfixture(4),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 1,padx = 20,pady= 15)
        week05 = Button(self.window10,text = "Week05",command= lambda :weeklyfixture(5),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 1,padx = 20,pady= 15)
        week06 = Button(self.window10,text = "Week06",command= lambda :weeklyfixture(6),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 1,padx = 20,pady= 15)
        week07 = Button(self.window10,text = "Week07",command= lambda :weeklyfixture(7),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 2,padx = 20,pady= 15)
        week08 = Button(self.window10,text = "Week08",command= lambda :weeklyfixture(8),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 2,padx = 20,pady= 15)
        week09 = Button(self.window10,text = "Week09",command= lambda :weeklyfixture(9),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 2,padx = 20,pady= 15)
        week10 = Button(self.window10,text = "Week10",command= lambda :weeklyfixture(10),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 2,padx = 20,pady = 15)
        week11 = Button(self.window10,text = "Week11",command= lambda :weeklyfixture(11),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 2,padx = 20,pady = 15)
        week12 = Button(self.window10,text = "Week12",command= lambda :weeklyfixture(12),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 2,padx = 20,pady = 15)
        week13 = Button(self.window10,text = "Week13",command= lambda :weeklyfixture(13),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 3,padx = 20,pady = 15)
        week14 = Button(self.window10,text = "Week14",command= lambda :weeklyfixture(14),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 3,padx = 20,pady = 15)
        week15 = Button(self.window10,text = "Week15",command= lambda :weeklyfixture(15),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 3,padx = 20,pady = 15)
        week16 = Button(self.window10,text = "Week16",command= lambda :weeklyfixture(16),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 3,padx = 20,pady = 15)
        week17 = Button(self.window10,text = "Week17",command= lambda :weeklyfixture(17),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 3,padx = 20,pady = 15)
        week18 = Button(self.window10,text = "Week18",command= lambda :weeklyfixture(18),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 3,padx = 20,pady = 15)
        week19 = Button(self.window10,text = "Week19",command= lambda :weeklyfixture(19),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 4,padx = 20,pady = 15)
        week20 = Button(self.window10,text = "Week20",command= lambda :weeklyfixture(20),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 4,padx = 20,pady = 15)
        week21 = Button(self.window10,text = "Week21",command= lambda :weeklyfixture(21),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 4,padx = 20,pady = 15)
        week22 = Button(self.window10,text = "Week22",command= lambda :weeklyfixture(22),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 4,padx = 20,pady = 15)
        week23 = Button(self.window10,text = "Week23",command= lambda :weeklyfixture(23),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 4,padx = 20,pady = 15)
        week24 = Button(self.window10,text = "Week24",command= lambda :weeklyfixture(24),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 4,padx = 20,pady = 15)
        week25 = Button(self.window10,text = "Week25",command= lambda :weeklyfixture(25),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 1, column = 5,padx = 20,pady = 15)
        week26 = Button(self.window10,text = "Week26",command= lambda :weeklyfixture(26),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 2, column = 5,padx = 20,pady = 15)
        week27 = Button(self.window10,text = "Week27",command= lambda :weeklyfixture(27),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 3, column = 5,padx = 20,pady = 15)
        week28 = Button(self.window10,text = "Week28",command= lambda :weeklyfixture(28),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 4, column = 5,padx = 20,pady = 15)
        week29 = Button(self.window10,text = "Week29",command= lambda :weeklyfixture(29),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 5, column = 5,padx = 20,pady = 15)
        week30 = Button(self.window10,text = "Week30",command= lambda :weeklyfixture(30),width = 30,height = 5,bg = '#00331f',fg = '#f2f2f2').grid(row = 6, column = 5,padx = 20,pady = 15)
        self.window10.mainloop()
   
    def UserAccess(self):
        self.window2 = Tk()
        refresh()
        self.window2.title('User Menu')
        self.window2.resizable(0,0)
        self.window2.geometry('1300x830+100+10')
        self.window.withdraw()
        menubar = Menu(self.window2)
        self.window2.config(menu = menubar)
        ope = Menu(menubar, tearoff = 0)
        pre = Menu(menubar,tearoff=0)
        menubar.add_cascade(label = "Menu", menu = ope)
        menubar.add_cascade(label = "Previous Tables",menu = pre)
        exitmenu = Menu(menubar, tearoff = 0)
        ope.add_command(label = "View Team Squads",command = self.viewSquads)
        ope.add_command(label = "View Fixtures",command=self.USt)
        ope.add_command(label = "View Top Scorers",command = self.TopScorers)
        ope.add_command(label = "View Most Assist",command = self.Assists)
        ope.add_command(label = "View Most Yellow Cards",command = self.YellowCards)
        ope.add_command(label = "View Most Red Cards",command = self.RedCards)
        ope.add_command(label = "Logout",command=self.back1)
        pre.add_command(label = "2011 Table",command=lambda:self.TableView(11,16))
        pre.add_command(label = "2012 Table",command=lambda:self.TableView(12,16))
        pre.add_command(label = "2013 Table",command=lambda:self.TableView(13,16))
        pre.add_command(label = "2014 Table",command=lambda:self.TableView(14,16))
        pre.add_command(label = "2015 Table",command=lambda:self.TableView(15,16))
        pre.add_command(label = "2016 Table",command=lambda:self.TableView(16,18))
        pre.add_command(label = "2017 Table",command=lambda:self.TableView(17,18))
        menubar.add_cascade(label = "Exit", menu = exitmenu)
        exitmenu.add_command(label = "Quit", command = self.window2.destroy)
        frame2 = Frame(self.window2)
        frame2.pack()
        l=Label(frame2,text = "       Welcome to Ethiopian Premier League\nThe Ethiopian Premier League is the top association football division in Ethiopi\na. Regulated by the Ethiopian Football Federation, it was created in 1997 (1990 \nE.C.) replacing the former first division (est.1944). Contested by sixteen clubs\n, it operates on a system of promotion and relegation with the other secondary a\nnd tertiary leagues in Ethiopia. The league has been an annual competition since\n the 1997-98 season with Saint George S.C. emerging as the country's leading clu\nb in this era with 14 titles (29 first division titles overall).                                         ").pack()
        frame = Frame(self.window2)
        frame.pack()
        Label(frame2,text="Current Season Standings",font=('Times',20)).pack()
        tableView(frame,0,18)
        self.window2.mainloop() 
    def TopScorers(self):
        self.window12 = Tk()
        recordView(self.window12,10,20)
        self.window12.mainloop()
    def Assists(self):
        self.window12 = Tk()
        recordView(self.window12,27,20)
        self.window12.mainloop()
    def YellowCards(self):
        self.window12 = Tk()
        recordView(self.window12,29,20)
        self.window12.mainloop()
    def RedCards(self):
        self.window12 = Tk()
        recordView(self.window12,28,20)
        self.window12.mainloop()
    def TableView(self,index,height):
        self.window5 = Tk()
        tableView(self.window5,index,height)
        self.window5.mainloop()

    def viewSquads(self):
        self.window8 = Tk()
        self.window8.title('View Squads') 
        Button(self.window8,text="Ethiopian Coffee S.C.",command=lambda:self.DisplaySquad(3,26),width = 25).grid(row=0,column=0)
        Button(self.window8,text="Wolaitta Dicha S.C.",command=lambda:self.DisplaySquad(4,30),width = 25).grid(row=1,column=0)
        Button(self.window8,text="Dedebit F.C.",command=lambda:self.DisplaySquad(5,29),width = 25).grid(row=2,column=0)
        Button(self.window8,text="Debub Police S.C.",command=lambda:self.DisplaySquad(6,23),width = 25).grid(row=3,column=0)
        Button(self.window8,text="Welwalo Adigrat University F.C.",command=lambda:self.DisplaySquad(7,27),width = 25).grid(row=4,column=0)
        Button(self.window8,text="Fasil Kenema S.C.",command=lambda:self.DisplaySquad(8,20),width = 25).grid(row=5,column=0)
        Button(self.window8,text="Shire Endaselassie F.C.",command=lambda:self.DisplaySquad(9,16),width = 25).grid(row=6,column=0)
        Button(self.window8,text="Adama City F.C.",command=lambda:self.DisplaySquad(18,19),width = 25).grid(row=7,column=0)
        Button(self.window8,text="Jimma Abba Jifar F.C.",command=lambda:self.DisplaySquad(19,25),width = 25).grid(row=0,column=1)
        Button(self.window8,text="Saint George S.C.",command=lambda:self.DisplaySquad(20,28),width = 25).grid(row=1,column=1)
        Button(self.window8,text="Bahir Dar Kenema F.C.",command=lambda:self.DisplaySquad(21,20),width = 25).grid(row=2,column=1)
        Button(self.window8,text="Sidama Coffee S.C.",command=lambda:self.DisplaySquad(22,24),width = 25).grid(row=3,column=1)
        Button(self.window8,text="Mekele 70 Enderta S.C.",command=lambda:self.DisplaySquad(23,25),width = 25).grid(row=4,column=1)
        Button(self.window8,text="Hawassa City S.C.",command=lambda:self.DisplaySquad(24,18),width = 25).grid(row=5,column=1)
        Button(self.window8,text="Defence Force S.C.",command=lambda:self.DisplaySquad(25,28),width = 25).grid(row=6,column=1)
        Button(self.window8,text="Dire Dawa City S.C.",command=lambda:self.DisplaySquad(26,27),width = 25).grid(row=7,column=1)
        self.window8.mainloop()
    def DisplaySquad(self,index,height):
        viewSquads(index,height)


Access()






        
        
